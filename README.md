# oBNVDs publique

## Introduction

This is a restricted (no identifications) version of the codes that produce the open spatialization of the French pesticides sales data ([BNV-D](https://ventes-produits-phytopharmaceutiques.eaufrance.fr/)) following the procedure described in [Ramalanjaona et al. (2020)](https://odr.inrae.fr/intranet/carto_joomla/files/Note_methodo_calculcoeff_vf.pdf).

## Data used

The spatialization builds on these data:

-   BNV-D
-   Hexaposte
-   RPG
-   RPG complété
-   BD TOPO
-   Statisques agricoles annuelles
-   E-Phy

## Software

The calculation are made using **PostgreSQL** and its spatial extension **PostGIS**. Orchestration and other operations are done via **R** and **python**.

## Description

The code here

## Installation

No particular installation for these scripts, a **PostrgreSQL** server should be set up and the **PostGIS** extension is necessary if spatial information is to be manipulated. For **R** the packages are: `tidyverse, RPostgreSQL, here, readxl` and some features work with the **R Studio** software. For **python** the packages are: `configparser, datetime, psycopg2`.

## Usage

The spatialization of the BNV-D data allows the attribution of phytopharmaceutical products and substances quantities at the plot level. This scale is useful for intersecting with other spatial units and further reagregation. The results are not intended to be interpreted at the plot level, they are meaningless at this scale.

## Support

We do not provide support for the code except for common projects. Address any queries to US ODR, INRAE, [bnvd-spatialisation@inrae.fr](mailto:bnvd-spatialisation@inrae.fr) .

## Roadmap

Future developments are to come.

## Contributing

We are accepting feedback on the code.

## Authors and acknowledgment

The code is written by Anna Lungarska, Lovasoa Ramalanjaona and is property of INRAE, France. It builds on the work of Camille Truche, Claire Séard, Eva Groshens, Marie Carles, Pascal Filippi, and Olivier Lision under the supervision by Eric Cahuzac and Thomas Poméon. It has also been made possible by the work of Pierre Cantelaube and Benjamin Lardot.

## License

Distributed under GNU AGPLv3 license.

## References

Cantelaube P. et Lardot B., (2021) [Construction d’une base de données géographiques à échelle fine exhaustive sur l’occupation agricole du sol : Le « RPG complété ». Partie 2 : Attribution des cultures aux parcelles susceptibles d’accueillir les surfaces agricoles hors RPG](https://hal.inrae.fr/hal-03818008). Version 1. Note Méthodologue US ODR, INRAE, février 2021, <https://hal.inrae.fr/hal-03818008>

Lardot B., Cantelaube P., Carles M., Séard C., Truche C. et Poméon T., (2021). [Construction d’une base de données géographiques exhaustive à échelle fine sur l’occupation agricole du sol : Le « RPG complété ». Partie 1 : Production de la couche géographique des parcelles susceptibles d’accueillir les surfaces agricoles hors RPG](https://hal.inrae.fr/hal-03647500). Version 1. Note Méthodologique US ODR, INRAE, février 2021, <https://hal.inrae.fr/hal-03647500>

Ramalanjaona L., Poméon T., Ballot R., Barbu C., Bougon N., Fernandez F., Martin P., Mohamed H. (2020). [Mise à jour du calcul des coefficients de répartition spatiale des données de la BNVd](https://hal.inrae.fr/hal-03594944/). Note méthodologique ODR, 2020. <https://hal.inrae.fr/hal-03594944/>
