import configparser

config = configparser.ConfigParser()
config.read("/config.ini")

usern = config['credentials']['username']
pw = config['credentials']['password']
dbase = ""
dbase_rpg = ""

import datetime
import psycopg2
# Connect to an existing database

conn = psycopg2.connect(
    host="localhost",
    database=dbase,
    user=usern,
    password=pw)

# Open a cursor to perform database operations
cur = conn.cursor()

annees = range(2015, 2021, 1)

for a in annees:
    
    bdd_target = "obnvds_" + str(a)
    print(str(a))

    cur.execute("""
    SET ROLE gr_db_atelier_ecophyto;
    """)

    cur.execute("""
    SELECT EXISTS (
    SELECT FROM 
        pg_tables
    WHERE 
        schemaname = '""" + bdd_target + """' AND 
        tablename  = 'com_adm_occsol'
    );
    """)
    
    table_ex = cur.fetchall()

    if (table_ex[0][0] == False):
        cur.execute("""
        CREATE TABLE """ + bdd_target + """.com_adm_occsol AS (
          WITH regs AS (
            SELECT DISTINCT code_region region, 
                code_commune_insee com_adm
            FROM donnees_references.com_dep_reg
          )
         SELECT com_adm, region, cp_adm, occsol, SUM(surf_ha) surf_ha
         FROM """ + bdd_target + """.parcelles 
         INNER JOIN regs USING(com_adm)
         GROUP BY com_adm, region, cp_adm, occsol
         );
         """)
        
        now = datetime.datetime.now()
        
        cur.execute("""
        COMMENT ON TABLE """ + bdd_target + """.com_adm_occsol IS 
        'Schema created on """ + now.strftime("%d/%m/%Y, %H:%M:%S") + """ by """ + usern + """';
        """)
        
        cur.execute("""
        CREATE INDEX cp_occs_adms ON """ + bdd_target + """.com_adm_occsol (region, cp_adm, occsol);
        """)
        

    cur.execute("""
    SELECT EXISTS (
    SELECT FROM 
        pg_tables
    WHERE 
        schemaname = '""" + bdd_target + """' AND 
        tablename  = 'com_adm_occsol_qsa'
    );
    """)
    
    table_ex = cur.fetchall()

    if (table_ex[0][0] == True):
    
        cur.execute("""
        DROP TABLE """ + bdd_target + """.com_adm_occsol_qsa;
        """)
        

    cur.execute("""CREATE TABLE """ + bdd_target + """.com_adm_occsol_qsa AS 
        SELECT cpoccs.com_adm, cpoccs.cp_adm, cpoccs.occsol,
            bnvd.substance, cas, classification, classification_mention,
            code_sandre_substance, fonction_substance, 
            SUM(cpoccs.surf_ha * quantite_substance * coef) qsa
         FROM """ + bdd_target + """.com_adm_occsol cpoccs
         INNER JOIN """ + bdd_target + """.coef_amm_cp coefs USING(region, cp_adm, occsol)
         INNER JOIN """ + bdd_target + """.bnvd_sa_cor bnvd
         ON (cpoccs.cp_adm = bnvd.code_postal_acheteur 
                AND coefs.amm = bnvd.amm
                AND coefs.unite = bnvd.conditionnement)
        GROUP BY cpoccs.com_adm, cpoccs.cp_adm, cpoccs.occsol, bnvd.substance, cas, 
          classification, classification_mention,
          code_sandre_substance, fonction_substance;""")
    
    now = datetime.datetime.now()

    cur.execute("""
    COMMENT ON TABLE """ + bdd_target + """.com_adm_occsol_qsa IS 
        'Table created on """ + now.strftime("%d/%m/%Y, %H:%M:%S") + """ by """ + usern + """.
         Precalcul at the commune level';
    """)
    
    cur.execute("""CREATE INDEX com_adm_qsax ON """ + bdd_target + """.com_adm_occsol_qsa (com_adm);""")
    
    cur.execute("""
    SELECT EXISTS (
    SELECT FROM 
        pg_tables
    WHERE 
        schemaname = '""" + bdd_target + """' AND 
        tablename  = 'com_adm_occsol_amm'
    );
    """)
    
    table_ex = cur.fetchall()

    if (table_ex[0][0] == True):
    
        cur.execute("""
        DROP TABLE """ + bdd_target + """.com_adm_occsol_amm;
        """)
        

    cur.execute("""CREATE TABLE """ + bdd_target + """.com_adm_occsol_amm AS 
        SELECT cpoccs.com_adm, cpoccs.cp_adm, cpoccs.occsol,
            bnvd.amm, conditionnement, eaj, 
            SUM(cpoccs.surf_ha * quantite_produit * coef) quantite_produit
         FROM """ + bdd_target + """.com_adm_occsol cpoccs
         INNER JOIN """ + bdd_target + """.coef_amm_cp coefs USING(region, cp_adm, occsol)
         INNER JOIN """ + bdd_target + """.bnvd_amm_cor bnvd
         ON (cpoccs.cp_adm = bnvd.code_postal_acheteur 
                AND coefs.amm = bnvd.amm
                AND coefs.unite = bnvd.conditionnement)
        GROUP BY cpoccs.com_adm, cpoccs.cp_adm, cpoccs.occsol, bnvd.amm, conditionnement, eaj;""")
    
    now = datetime.datetime.now()

    cur.execute("""
    COMMENT ON TABLE """ + bdd_target + """.com_adm_occsol_amm IS 
        'Table created on """ + now.strftime("%d/%m/%Y, %H:%M:%S") + """ by """ + usern + """.
         Precalcul at the commune level';
    """)
    
    cur.execute("""CREATE INDEX com_adm_ammx ON """ + bdd_target + """.com_adm_occsol_amm (com_adm);""")

    conn.commit()


#Close communication with the database
cur.close()
conn.close()
