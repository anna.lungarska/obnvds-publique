import configparser

config = configparser.ConfigParser()
config.read("config.ini")

usern = config['credentials']['username']
pw = config['credentials']['password']
dbase = "" # working database
dbase_rpg = "" # database source if any 

import datetime
import psycopg2
# Connect to an existing database

conn = psycopg2.connect(
    host="localhost",
    database=dbase,
    user=usern,
    password=pw)

# Open a cursor to perform database operations
cur = conn.cursor()

annees = [2015, 2016, 2017, 2018, 2019, 2020]

for a in annees:
    
    annee = str(a)
    print(annee)

    cur.execute("""
    SET ROLE gr_db_atelier_ecophyto;
    """)

    cur.execute("""
    SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'obnvds_""" + annee + """';
    """)
    
    schema_ex = cur.fetchall()

    if (len(schema_ex) == 0):
        cur.execute("""
        CREATE SCHEMA IF NOT EXISTS obnvds_""" + annee + """ AUTHORIZATION gr_db_atelier_ecophyto;
        """)
        
        now = datetime.datetime.now()
        
        cur.execute("""
        COMMENT ON SCHEMA obnvds_""" + annee + """ IS 
        'Schema created on """ + now.strftime("%m/%d/%Y, %H:%M:%S") + """ by """ + usern + """';
        """)

    # Creer la table obnvds_[annee].parcelles avec les attributs suivants :
    # ilot : identifiant unique de la parcelle
    # geom : geometrie des polygones des parcelles
    # com_adm : code commune INSEE attribue a la parcelle
    # cp_adm : code postal attribue a la parcelle
    # occsol : groupe de culture/zna 
    # surf_ha : surface en hectare, calcul a partir de la geometrie
    cur.execute("""CREATE TABLE obnvds_""" + annee + """.parcelles AS
            
            ;
    """)
  
    now = datetime.datetime.now()

    cur.execute("""COMMENT ON TABLE obnvds_""" + annee + """.parcelles  
        IS 'creation : """ + now.strftime("%m/%d/%Y, %H:%M:%S") + """ by """ + usern + """ via le script 01.occsol.py';
    """)
  
  
    cur.execute("""ALTER TABLE obnvds_""" + annee + """.parcelles  ADD CONSTRAINT parcelles_pk PRIMARY KEY (ilot);""")
    cur.execute("""CREATE INDEX pc_geom_idx ON obnvds_""" + annee + """.parcelles USING gist (geom);""")
    cur.execute("""CREATE INDEX cp_occs_idx ON obnvds_""" + annee + """.parcelles USING btree (cp_adm, occsol);""")

    conn.commit()


#Close communication with the database
cur.close()
conn.close()
